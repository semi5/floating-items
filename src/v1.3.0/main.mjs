export function setup(ctx) {
    ctx.onModsLoaded(ctx => {
        const id = 'floating-items';

        // settings
        const SETTING_GENERAL = ctx.settings.section('General');
        const SETTING_TOOLTIP = ctx.settings.section('Tooltip');

        // config
        let watchList = [];

        let hideDisplay = false;
        let isCompact = false;
        let isCollapsed = false;

        // html elements
        let htmlHover = null;
        let htmlRow = null;

        // utils
        const numberColor = (number) => {
            let colorfix = '';

            if (number >= 1000 && number < 1000000) colorfix = '#1eff00';
            else if (number >= 1000000 && number < 100000000) colorfix = '#45caf7';
            else if (number >= 100000000 && number < 1000000000) colorfix = '#b755fc';
            else if (number > 1000000000) colorfix = '#ff8000';

            if (colorfix != '')
                return `<span style="color: ${colorfix};">` + formatNumber(number) + '</span>';

            return formatNumber(number);
        };

        // script
        const load = () => {
            isCompact = SETTING_GENERAL.get('compact-mode');
            hideDisplay = SETTING_GENERAL.get('hide-display');
            isCollapsed = ctx.characterStorage.getItem('isCollapsed') ? true : false;

            // v2
            const L2List = ctx.characterStorage.getItem('L2');
            if (L2List) {
                watchList = L2List.map(data => decodeSlotData(data));
            }

            // v1
            const storedWatchList = ctx.characterStorage.getItem('watchList');
            if (!L2List && storedWatchList) {
                watchList = storedWatchList.map(itemID => new DockItem(itemID));
            }
        };

        const save = () => {
            ctx.characterStorage.removeItem('watchList');

            ctx.characterStorage.setItem('isCollapsed', isCollapsed);
            ctx.characterStorage.setItem('L2', watchList.map(slot => slot.encode()));
        };

        const decodeSlotData = (data) => {
            switch (data.type) {
                case 0: return DockItem.decode(data);
                case 1: return DockEquipment.decode(data);
                case 2: return DockFood.decode(data);
                case 3: return DockCurrency.decode(data);
            }
            return null;
        };

        // option toggles
        const toggleCompact = () => {
            isCompact = !isCompact;
            updateCompact();
        };

        const toggleShowDisplay = () => {
            hideDisplay = !hideDisplay;
            updateDockDisplay();
        };

        const toggleCollapseItems = () => {
            isCollapsed = !isCollapsed;
            updateCollapse();
            save();
        };

        const triggerTooltipUpdate = () => {
            setTimeout(function () {
                slotUpdateAll();
            }, 100);
        };

        const init = () => {
            load();

            // build html elements
            const itemWatcherFloating = $(`<div id="${id}-container" class="${isCompact ? 'compact' : ''}">
                <div class="box">
                    <div class="actions">
                        <div class="btn btn-outline-success ${id}-add-swal">+</div>
                        <div class="btn btn-outline-info mt-1 ${id}-toggle-display">&lt;</div>
                    </div>
                    <div id="${id}-row"></div>
                </div>
            </div>`);
            $('#page-container').append(itemWatcherFloating);

            $(`.${id}-add-swal`).on("click", showItemSWAL);
            $(`.${id}-toggle-display`).on("click", toggleCollapseItems);

            const itemWatcherBankButton = $(`<div style="position:absolute;right:3px;top:0px;"><button class="${id}-toggle-bank btn btn-sm btn-secondary"><i id="bank-item-${id}-icon" class="fa fa-eye text-success"></i></button></div>`);
            $('bank-selected-item-menu').find('.fa-lock').parent().parent().parent().append(itemWatcherBankButton);
            $(`.${id}-toggle-bank`).on("click", clickBank);

            htmlHover = $(`#${id}-container`);
            htmlRow = $(`#${id}-row`);

            // draggable sorting
            Sortable.create(htmlRow.get(0), {
                delay: 400,
                delayOnTouchOnly: true,
                onEnd: (evt) => {
                    tippy.hideAll();
                    watchList.splice(evt.newIndex, 0, watchList.splice(evt.oldIndex, 1)[0]);
                    save();
                },
                onMove: () => {
                    tippy.hideAll();
                },
                onChoose: (evt) => {
                    tippy.hideAll();
                }
            });

            buildDockSlots();
            updateDockDisplay();
            updateCollapse();
        };

        // dock
        const buildDockSlots = () => {
            htmlRow.empty();

            watchList.forEach(slot => {
                htmlRow.append(slot.node);
            });
        };

        const addSlot = (slot) => {
            watchList.push(slot);
            htmlRow.append(slot.node);
            save();
        };

        const removeSlot = (slot) => {
            for (let i = watchList.length - 1; i >= 0; i--) {
                if (watchList[i] === slot) {
                    watchList.splice(i, 1);
                    slot.node.parentElement.removeChild(slot.node);
                }
            }

            save();
        };

        // display toggles
        const updateDockDisplay = () => {
            if (hideDisplay) {
                htmlHover.addClass('d-none');
                return;
            }
            htmlHover.removeClass('d-none');
        };

        const updateCompact = () => {
            if (isCompact) {
                htmlHover.addClass('compact');
            } else {
                htmlHover.removeClass('compact');
            }
            slotUpdateAll();
        };

        const updateCollapse = () => {
            if (isCollapsed) {
                htmlRow.addClass('d-none');
                $(`.${id}-toggle-display`).html('&gt;');
            } else {
                htmlRow.removeClass('d-none');
                $(`.${id}-toggle-display`).html('&lt;');
            }
        };

        // slot updates
        const slotUpdate = (classType, data = null) => {
            watchList.forEach(slot => {
                if (slot instanceof classType) {
                    slot.update(data);
                }
            });
        };

        const slotUpdateAll = () => {
            watchList.forEach(slot => {
                slot.update();
            });
        };

        const slotFind = (ClassType, data) => {
            for (let i = 0; i < watchList.length; i++) {
                let slot = watchList[i];
                if (slot instanceof ClassType && slot.compare(data)) {
                    return slot;
                }
            }
            return null;
        };

        // bank - item
        const clickBank = (evt) => {
            bankToggle(game.bank.selectedBankItem.item);
        };

        const bankToggle = (item) => {
            let idx = slotFind(DockItem, item);

            if (idx != null)
                removeSlot(idx);
            else
                addSlot(new DockItem(item.id));
        };

        const equipmentToggle = (data) => {
            let idx = slotFind(DockEquipment, data);

            if (idx != null)
                removeSlot(idx);
            else
                addSlot(new DockEquipment(data.slot, data.set));
        };

        const foodToggle = (data) => {
            let idx = slotFind(DockFood, data);

            if (idx != null)
                removeSlot(idx);
            else
                addSlot(new DockFood(data.slot));
        };

        const currencyToggle = (currency) => {
            let idx = slotFind(DockCurrency, currency);

            if (idx != null)
                removeSlot(idx);
            else
                addSlot(new DockCurrency(currency.id));
        };

        // -- swal ------------------------
        let SWAL_NODE;

        const buildSWALNode = () => {
            // data
            const orderMap = new Map(game.bank.defaultSortOrder.map((val, i) => [val, i]));
            const itemList = game.items.allObjects.filter(item => item.type != "" && !item.golbinRaidExclusive).sort((a, b) => (orderMap.get(a) || Infinity) - (orderMap.get(b) || Infinity));

            const fuse = new Fuse(itemList, {
                shouldSort: false,
                tokenize: true,
                matchAllTokens: true,
                findAllMatches: true,
                threshold: 0.1,
                minMatchCharLength: 3,
                keys: ['name', 'category', 'type', 'description'],
            });

            // bank - search
            const searchElmMap = new Map();
            const searchItems = new Set();

            const SEARCH_FIELD = createElement('input', {
                className: 'form-control',
                attributes: [
                    ['type', 'text'],
                    ['placeholder', 'Search...']
                ]
            });
            SEARCH_FIELD.onkeyup = function () {
                updateSearchItems();
            };

            const buildSearchItems = () => {
                const container = createElement('div', {
                    className: 'd-flex flex-wrap justify-content-center mt-2'
                });

                itemList.forEach((item) => {
                    const selectItem = createElement('div', {
                        className: "m-1 p-2 bg-combat-inner-dark rounded pointer-enabled d-none",
                        parent: container,
                        children: [createElement('img', {
                            className: 'resize-32',
                            attributes: [
                                ['src', item.media],
                                ['alt', item.name]
                            ],
                        })]
                    });

                    selectItem.onclick = function (e) {
                        bankToggle(item);
                    }

                    tippy(selectItem, {
                        animation: false,
                        allowHTML: true,
                        content: createItemInformationTooltip(item),
                        zIndex: 999999 // SWAL z-index is set to a high number.
                    });

                    container.append(selectItem);
                    searchElmMap.set(item, selectItem);
                });

                return container;
            };

            const updateSearchTerms = () => {
                let query = SEARCH_FIELD.value;
                searchItems.clear();

                if (query.length >= 3) {
                    fuse.search(query).forEach(result => {
                        searchItems.add(result);
                    });
                }
            };

            const updateSearchItems = () => {
                updateSearchTerms();

                itemList.forEach(item => {
                    const element = searchElmMap.get(item);
                    if (element === undefined) {
                        return;
                    }
                    if (searchItems.has(item)) {
                        showElement(element);
                    } else {
                        hideElement(element);
                    }
                });
            };

            const BANK_TAB = createElement('div', {
                children: [SEARCH_FIELD, buildSearchItems()]
            });

            // equipment + food
            const buildEquipmentItems = () => {
                let items = [];

                // equipment
                let activeEquipmentSet = -1;

                const getEquipmentSetName = (index) => {
                    if (index < 0) return "Current Set";
                    return `Set ${index + 1}`;
                }

                items.push(createElement('div', { children: `Equipment` }));

                let equipmentMaxSets = Math.max(10, game.combat.player.numEquipSets);
                const equipmentBtnGroup = createElement('select', { className: "form-control mt-2 mb-2" });
                equipmentBtnGroup.onchange = function (e) {
                    activeEquipmentSet = parseInt(this.options[this.selectedIndex].value);
                }

                for (let i = -1; i < equipmentMaxSets; i++) {
                    createElement('option', {
                        parent: equipmentBtnGroup,
                        children: getEquipmentSetName(i),
                        attributes: [['value', i]]
                    });
                }
                items.push(equipmentBtnGroup);

                const equipment_container = createElement('div', { className: 'floating-items-equipment-box row no-gutters text-center' });
                const gridSize = EquipmentSlot.getGridSize();
                const numCols = gridSize.cols.max - gridSize.cols.min + 1;
                const numRows = gridSize.rows.max - gridSize.rows.min + 1;
                const colOffset = 1 - gridSize.cols.min;
                const rowOffset = 1 - gridSize.rows.min;
                equipment_container.style.gridTemplateColumns = `repeat(${numCols}, auto)`;
                equipment_container.style.gridTemplateRows = `repeat(${numRows}, auto)`;
                game.equipmentSlots.forEach((slot) => {
                    const img = slot.emptyMedia;
                    const gridSlot = createElement('div', { parent: equipment_container });
                    gridSlot.style.gridColumn = `${slot.gridPosition.col + colOffset}`;
                    gridSlot.style.gridRow = `${slot.gridPosition.row + rowOffset}`;

                    const gridImg = createElement('img', {
                        className: 'combat-equip-img border border-2x border-rounded-equip border-combat-outline p-1 pointer-enabled',
                        parent: gridSlot,
                        attributes: [
                            ['src', img],
                            ['alt', slot.name]
                        ],
                    });

                    gridImg.onclick = function (e) {
                        equipmentToggle({ "set": activeEquipmentSet, "slot": slot.id });
                    }
                });

                items.push(equipment_container);

                items.push(createElement('hr'));

                // food
                items.push(createElement('div', { children: `Food Slots` }));
                const food_container = createElement('div', {
                    className: 'd-flex flex-wrap justify-content-center mt-2 mb-4'
                });
                const food_groups = ['Current Slot', 'Slot 1', 'Slot 2', 'Slot 3'];
                food_groups.forEach((title, index) => {
                    const selectItem = createElement('div', {
                        className: "m-1 p-2 bg-combat-inner-dark rounded pointer-enabled",
                        parent: food_container,
                        children: [createElement('small', { children: title })]
                    });

                    selectItem.onclick = function (e) {
                        foodToggle({ "slot": index - 1 });
                    }
                });
                items.push(food_container);

                return items;
            }
            const EQUIPMENT_TAB = createElement('div', {
                children: buildEquipmentItems()
            });

            // currency
            const buildCurrencyItems = () => {
                let items = [];

                items.push(createElement('div', { children: `Currency` }));
                const currency_container = createElement('div', {
                    className: 'd-flex flex-wrap justify-content-center mt-2 mb-4'
                });
                game.currencies.forEach((currency) => {
                    const selectItem = createElement('div', {
                        className: "m-1 p-2 bg-combat-inner-dark rounded pointer-enabled",
                        parent: currency_container,
                        children: [createElement('img', {
                            className: 'resize-32',
                            attributes: [
                                ['src', currency.media],
                                ['alt', currency.name]
                            ],
                        })]
                    });

                    selectItem.onclick = function (e) {
                        currencyToggle(currency);
                    }
                });
                items.push(currency_container);

                return items;
            }
            const CURRENCY_TAB = createElement('div', {
                children: buildCurrencyItems()
            });

            // menu
            const TAB_CONTAINERS = [BANK_TAB, EQUIPMENT_TAB, CURRENCY_TAB];
            const TAB_BUTTONS = [["Bank Items", BANK_TAB], ["Equipment", EQUIPMENT_TAB], ["Currency", CURRENCY_TAB]];
            const TAB_MENU = createElement('div', {
                className: 'mb-3',
                children: TAB_BUTTONS.map(data => {
                    const btn = createElement('button', {
                        className: 'btn btn-sm btn-primary m-2',
                        children: data[0]
                    });

                    btn.onclick = function (e) {
                        changeSWALTab(data[1]);
                    }

                    return btn;
                })
            });

            const changeSWALTab = (page) => {
                TAB_CONTAINERS.forEach(tab => {
                    tab.classList.add('d-none');
                });

                page.classList.remove('d-none');
            }

            const reset = () => {
                changeSWALTab(BANK_TAB);
                SEARCH_FIELD.value = '';
                updateSearchItems();
            }

            const node = createElement('div', {
                children: [TAB_MENU, BANK_TAB, EQUIPMENT_TAB, CURRENCY_TAB]
            });

            return {
                reset,
                node
            }
        }

        const showItemSWAL = () => {
            if (SWAL_NODE == null)
                SWAL_NODE = buildSWALNode();

            SWAL_NODE.reset();
            SwalLocale.fire({
                title: `Toggle Dock Item`,
                width: '60em',
                confirmButtonText: 'Close',
                html: SWAL_NODE.node
            });
        };

        // settings
        const buildSwitch = (name, label, defaultValue, onChange) => {
            return {
                'type': 'switch',
                'name': name,
                'label': label,
                'default': defaultValue,
                'onChange': onChange
            }
        };

        SETTING_GENERAL.add([
            buildSwitch('hide-display', 'Hide Floating Items', false, toggleShowDisplay),
            buildSwitch('compact-mode', 'Enable Compact Mode', true, toggleCompact)
        ]);

        SETTING_TOOLTIP.add([
            {
                'type': 'label',
                'name': `tooltip-info`,
                'label': `Item Values shown within the tooltip show the total sum of the entire stack and not per item.`
            },
            buildSwitch('tooltip-colors', 'Use Number Colors', true, triggerTooltipUpdate),
            buildSwitch('tooltip-name', 'Show Item Name', true, triggerTooltipUpdate),
            buildSwitch('tooltip-quantity', 'Show Quantity', false, triggerTooltipUpdate),
            buildSwitch('tooltip-hp', 'Show HP Restoration', false, triggerTooltipUpdate),
            buildSwitch('tooltip-sell-value', 'Show Item Sell Value', true, triggerTooltipUpdate),
            buildSwitch('tooltip-item-alchemy-i', 'Show Item Alchemy I', false, triggerTooltipUpdate),
            buildSwitch('tooltip-item-alchemy-ii', 'Show Item Alchemy II', false, triggerTooltipUpdate),
            buildSwitch('tooltip-item-alchemy-iii', 'Show Item Alchemy III', true, triggerTooltipUpdate),
            buildSwitch('tooltip-item-alchemy-iv', 'Show Item Alchemy IV', true, triggerTooltipUpdate)
        ]);

        // classes
        class DockBase {
            get node() { return this.nodeElm; }

            build = () => {
                const node = $(this.slotHTML());
                const nodeIcon = node.find('.item-icon');
                const nodeQty = node.find('.item-qty');

                this.nodeElm = node.get(0);
                this.nodeIcon = nodeIcon.get(0);
                this.nodeQty = nodeQty.get(0);

                node.on("click", () => this.click());
                node.on("dblclick", () => this.dblClick());

                tippy(this.nodeElm, {
                    content: this.tippy(),
                    placement: "top",
                    allowHTML: true,
                    interactive: false,
                    animation: false,
                });
            }

            click = () => {
                this.update();
            }

            dblClick = () => {
                removeSlot(this);
            }

            cost = () => {
                return game.bank.getItemSalePrice(this.item) * this.quantity();
            }

            healing = () => {
                return game.combat.player.getFoodHealing(this.item) * this.quantity();
            }

            alchemy = (conversion) => {
                return game.altMagic.getAlchemyGP(this.item, conversion) * this.quantity();
            }

            qtyHTML = (number) => {
                if (isCompact) {
                    return formatNumber(number);
                }
                return numberWithCommas(number);
            }

            tippy = () => {
                const numberFunction = SETTING_TOOLTIP.get('tooltip-colors') ? numberColor : formatNumber;

                const isEmpty = (this.item === game.emptyEquipmentItem || this.item === game.emptyFoodItem);

                const itemName = SETTING_TOOLTIP.get('tooltip-name') ? (isEmpty ? 'Empty Slot' : `<div class="font-w600">${this.item.name}</div>`) : '';

                const displayItems = [...this.tippyExtra()];

                if (!isEmpty) {
                    if (SETTING_TOOLTIP.get('tooltip-quantity'))
                        displayItems.push(`<small class="badge-pill bg-secondary">${numberWithCommas(this.quantity())}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-sell-value'))
                        displayItems.push(`<img class="skill-icon-xxs" src="${cdnMedia('assets/media/main/coins.svg')}"> <small>${numberFunction(this.cost())}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-hp') && (this.item instanceof FoodItem))
                        displayItems.push(`<img class="skill-icon-xxs" src="${game.hitpoints.media}"> <small>${numberFunction(this.healing())}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-item-alchemy-i'))
                        displayItems.push(`<img class="skill-icon-xxs" src="${cdnMedia('assets/media/skills/magic/item_alchemy.svg')}"> <small>I: ${numberFunction(this.alchemy(0.4))}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-item-alchemy-ii'))
                        displayItems.push(`<img class="skill-icon-xxs" src="${cdnMedia('assets/media/skills/magic/item_alchemy.svg')}"> <small>II: ${numberFunction(this.alchemy(1))}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-item-alchemy-iii'))
                        displayItems.push(`<img class="skill-icon-xxs" src="${cdnMedia('assets/media/skills/magic/item_alchemy.svg')}"> <small>III: ${numberFunction(this.alchemy(1.6))}</small>`);

                    if (SETTING_TOOLTIP.get('tooltip-item-alchemy-iv'))
                        displayItems.push(`<img class="skill-icon-xxs" src="${cdnMedia('assets/media/skills/magic/item_alchemy.svg')}"> <small>IV: ${numberFunction(this.alchemy(2.5))}</small>`);
                }

                return `<div class="text-center">
                    ${itemName}
                    <div class="font-size-sm">
                        ${displayItems.join('<br>')}
                    </div>
                </div>`;
            }

            tippyExtra = () => {
                return [];
            }
        }

        class DockItem extends DockBase {
            constructor(itemID) {
                super();

                this.item = game.items.getObjectByID(itemID);
                this.build();
            }

            get id() { return this.item.id; }

            quantity = () => {
                return game.bank.getQty(this.item);
            }

            update = (data = null) => {
                if (this.item == data || data == null) {
                    this.nodeElm._tippy.setContent(this.tippy());
                    this.nodeQty.innerHTML = this.qtyHTML(this.quantity());
                }
            }

            compare = (data) => {
                return data != null && this.item.id == data.id;
            }

            click = () => {
                this.update();

                if (game.openPage.id == "melvorD:Bank") {
                    const bankItem = game.bank.items.get(this.item);
                    if (bankItem) {
                        game.bank.selectItemOnClick(this.item);
                    }
                }
                else if (game.openPage.id == "melvorD:AltMagic") {
                    if (game.altMagic.selectedSpell != null) {
                        if (game.altMagic.selectedSpell.id.indexOf("ItemAlchemy") !== -1 && $('alt-magic-item-menu').is(":visible")) {
                            game.altMagic.selectItemOnClick(this.item);
                        }
                    }
                }
            }

            slotHTML = () => {
                return `<div class="${id}-item pointer-enabled draggable" draggable="true">
                    <img src="${this.item.media}" class="item-icon">
                    <div class="font-size-sm text-white text-center">
                        <small class="badge-pill bg-secondary item-qty">${this.qtyHTML(this.quantity(this.item))}</small>
                    </div>
                </div>`;
            }

            // save
            encode = () => {
                return { "type": 0, "id": this.item.id };
            }

            static decode = (data) => {
                return new DockItem(data.id);
            }
        }

        class DockEquipment extends DockBase {
            constructor(slotID, setID = -1) {
                super();

                this.set = setID;
                this.slotID = slotID;
                this.slot = game.equipmentSlots.getObjectByID(this.slotID);
                this.equipment = this.getSlot();
                this.item = this.equipment.item;
                this.build();
            }

            get id() { return this.slot.type; }

            getSlot = () => {
                if (this.set >= 0 && this.set < game.combat.player.equipmentSets.length)
                    return game.combat.player.equipmentSets[this.set].equipment.equippedItems[this.slotID];

                return game.combat.player.equipment.equippedItems[this.slotID];
            }

            quantity = () => {
                return this.equipment.quantity;
            }

            update = (data = null) => {
                this.equipment = this.getSlot();
                this.item = this.equipment.item;
                this.nodeElm._tippy.setContent(this.tippy());
                this.nodeQty.innerHTML = this.qtyHTML(this.quantity());

                if (this.nodeIcon.src != this.mediaSource)
                    this.nodeIcon.src = this.mediaSource;
            }

            get mediaSource() {
                return this.item == game.emptyEquipmentItem ? this.slot.emptyMedia : this.item.media;
            }

            compare = (data) => {
                return data != null && this.set == data.set && this.slotID == data.slot;
            }

            slotHTML = () => {
                return `<div class="${id}-item equipment pointer-enabled draggable" draggable="true">
                    <div class="type"><img src="assets/media/bank/armour_helmet_steel.png"></div>
                    <img src="${this.mediaSource}" class="item-icon">
                    <div class="font-size-sm text-white text-center">
                        <small class="badge-pill bg-secondary item-qty">${this.qtyHTML(this.quantity(this.item))}</small>
                    </div>
                </div>`;
            }

            tippyExtra = () => {
                let extra = [];

                if (this.set >= 0)
                    extra.push(`<small class="text-primary">Set ${(this.set + 1)} - ${this.slot.emptyName}</small>`);
                else
                    extra.push(`<small class="text-primary">Current - ${this.slot.emptyName}</small>`);

                return extra;
            }

            // save
            encode = () => {
                return { "type": 1, "slot": this.slotID, "set": this.set };
            }

            static decode = (data) => {
                if (data.slot.indexOf(":") == -1)  // v1.2.2 -> v1.3.0 Changes
                    data.slot = `melvorD:${data.slot}`;

                return new DockEquipment(data.slot, data.set);
            }
        }

        class DockFood extends DockBase {
            constructor(slotID = -1) {
                super();

                this.slotID = slotID;
                this.slot = this.getSlot();
                this.item = this.slot.item;
                this.build();
            }

            get id() { return this.slot.type; }

            getSlot = () => {
                if (this.slotID >= 0 && this.slotID < game.combat.player.food.maxSlots)
                    return game.combat.player.food.slots[this.slotID];

                return game.combat.player.food.slots[game.combat.player.food.selectedSlot];
            }

            quantity = () => {
                return this.slot.quantity;
            }

            update = (data = null) => {
                this.slot = this.getSlot();
                this.item = this.slot.item;
                this.nodeElm._tippy.setContent(this.tippy());
                this.nodeQty.innerHTML = this.qtyHTML(this.quantity());

                if (this.nodeIcon.src != this.item.media)
                    this.nodeIcon.src = this.item.media;
            }

            compare = (data) => {
                return data != null && data.slot == this.slotID;
            }

            slotHTML = () => {
                return `<div class="${id}-item food pointer-enabled draggable" draggable="true">
                    <div class="type"><img src="assets/media/bank/banana_bread.png"></div>
                    <img src="${this.item.media}" class="item-icon">
                    <div class="font-size-sm text-white text-center">
                        <small class="badge-pill bg-secondary item-qty">${this.qtyHTML(this.quantity(this.item))}</small>
                    </div>
                </div>`;
            }

            tippyExtra = () => {
                let extra = [];

                if (this.slotID >= 0)
                    extra.push(`<small class="text-primary">Food Slot ${(this.slotID + 1)}</small>`);
                else
                    extra.push(`<small class="text-primary">Current Food</small>`);

                return extra;
            }

            // save
            encode = () => {
                return { "type": 2, "slot": this.slotID };
            }

            static decode = (data) => {
                return new DockFood(data.slot);
            }
        }

        class DockCurrency extends DockBase {
            constructor(currencyID) {
                super();

                this.currency = game.currencies.getObjectByID(currencyID);
                this.build();
            }

            get id() { return this.currency.id; }

            quantity = () => {
                return this.currency.amount;
            }

            update = (data = null) => {
                if (this.currency == data || data == null) {
                    this.nodeElm._tippy.setContent(this.tippy());
                    this.nodeQty.innerHTML = this.qtyHTML(this.quantity());
                }
            }

            compare = (data) => {
                return data != null && this.currency.id == data.id;
            }

            click = () => {
                this.update();
            }

            tippy = () => {
                const itemName = SETTING_TOOLTIP.get('tooltip-name') ? `<div class="font-w600">${this.currency.name}</div>` : '';

                const displayItems = [...this.tippyExtra()];

                if (SETTING_TOOLTIP.get('tooltip-quantity'))
                    displayItems.push(`<small class="badge-pill bg-secondary">${numberWithCommas(this.quantity())}</small>`);

                return `<div class="text-center">
                    ${itemName}
                    <div class="font-size-sm">
                        ${displayItems.join('<br>')}
                    </div>
                </div>`;
            }

            slotHTML = () => {
                return `<div class="${id}-item pointer-enabled draggable" draggable="true">
                    <img src="${this.currency.media}" class="item-icon">
                    <div class="font-size-sm text-white text-center">
                        <small class="badge-pill bg-secondary item-qty">${this.qtyHTML(this.quantity(this.currency))}</small>
                    </div>
                </div>`;
            }

            // save
            encode = () => {
                return { "type": 3, "id": this.currency.id };
            }

            static decode = (data) => {
                return new DockCurrency(data.id);
            }
        }

        // hook
        ctx.onInterfaceReady(ctx => {
            init();

            game.bank.on('itemChanged', (event) => {
                slotUpdate(DockItem, event.item);
            });

            ctx.patch(Player, 'render').before(function () {
                if (this.renderQueue.equipment || this.renderQueue.equipmentSets) {
                    slotUpdate(DockEquipment);
                }
                if (this.renderQueue.food) {
                    slotUpdate(DockFood);
                }
            });

            ctx.patch(Currency, 'render').before(function () {
                if (this.renderRequired) {
                    slotUpdate(DockCurrency, this);
                }
            });
        });
    });
}